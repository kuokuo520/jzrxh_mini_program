import Http from '../../utils/Api.js'
Page({

    /**
     * 页面的初始数据
     */
    data: {
        signDay: {},
        banner: {},
        user: {}
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        this.getSignDay()
        this.getBanner()
        this.getUserInfo()
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    },
    getSignDay() {
        Http.Post('api/Sign/sign_day').then(res => {
            if (res.code === 1000) {
                this.setData({
                    signDay: res.data
                })
            }
        })
    },
    sign () {
        Http.Post('api/Sign/sign').then(res => {
           if (res.code === 1000) {
               wx.showToast({
                   title: `签到成功,获得${res.data.integral}积分`,
               })
               this.getSignDay()
           } else {
               wx.showToast({
                   title: res.message,
                   icon: 'none'
               })
           }
        })
    },
    getBanner () {
        Http.Post('api/Ad/getAdList', { position_code: 'rf3415r1'}).then(res => {
            if(res.code === 1000) {
                this.setData({banner: res.data})
            }
        })
    },
    getUserInfo() {
        Http.Post('api/user/userinfo').then(res => {
            if (res.code === -1004) {
                wx.showModal({
                    title: '',
                    content: '需要获取您的用户才能体验所有功能，点击确定跳转登录。',
                    success(res) {
                        if (res.confirm) {
                            wx.switchTab({
                                url: '/pages/my/my',
                            })
                        } else if (res.cancel) {

                        }
                    }
                })
            }
            this.setData({user: res.data})
        })
    },
    formSubmit (e) {
        let openid = ''
        wx.getStorage({
            key: 'openid',
            success:(res) => {
                openid = res.data
            },
        })
        setTimeout(() => {
            Http.Post('api/Wx/saveFormId', {
                openid: openid,
                formId: e.detail.formId
            }).then(res => {
                console.log(res)
                this.postCoupon()
            })
        }, 500)
    },
    postCoupon () {
        Http.Post('/api/Marketing/pushGoods').then(res => {
            console.log(res)
        })
    }
})