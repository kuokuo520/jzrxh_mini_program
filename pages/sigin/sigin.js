import Http from '../../utils/Api.js'
Page({

    /**
     * 页面的初始数据
     */
    data: {
        selected: [
            {
                date: '2019-5-21'
            }, {
                date: '2019-5-22'
            }, {
                date: '2019-5-24'
            }, {
                date: '2019-5-25'
            }
        ],
        banner: {},
        record: []
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) { 
        this.getBanner()
        this.getMySign()
    },
    /**
    * 日历是否被打开
    */
    bindselect(e) {
        console.log(e.detail.ischeck)
    },
    /**
     * 获取选择日期
     */
    bindgetdate(e) {
        let time = e.detail;
        console.log(time)

    },
    getBanner () {
        Http.Post('api/Ad/getAdList', { position_code: 'DSRFAa2'}).then(res => {
            if (res.code === 1000) {
                this.setData({
                    banner: res.data
                })
            }
        })
    },
    getMySign() {
        Http.Post('api/Sign/record', { pageSize: 99999}).then(res => {
            if(res.code === 1000) {
                this.setData({ record: res.data})
            }
        })
    }
})
