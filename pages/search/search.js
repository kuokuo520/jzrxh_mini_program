import Http from '../../utils/Api.js'
Page({

    /**
     * 页面的初始数据
     */
    data: {

    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {

    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    },
    search(e) {
        let searchText = e.detail.value
        Http.Post('api/Shop/shopList', {
            shop_name: searchText
        }).then(res => {
            if (res.code === 1000) {
                this.setData({
                    shopList: res.data
                })
                console.log(this.data.shopList)
            } else if (res.code === -1000) {
                wx.showToast({
                    title: res.message,
                    icon: 'none'
                })
            }
            this.triggerEvent('getShopList', res.data) //myevent自定义名称事件，父组件中使用
        })
    },
    focus() {
        this.setData({
            searchBg: true
        })
    },
    close() {
        this.setData({
            searchBg: false
        })
    }
})