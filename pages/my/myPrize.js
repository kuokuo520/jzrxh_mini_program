import Http from "../../utils/Api.js"
var QRCode = require('../../utils/weapp-qrcode.js')
const W = wx.getSystemInfoSync().windowWidth;
const rate = 750.0 / W;

// 300rpx 在6s上为 150px
const qrcode_w = 300 / rate;
Page({

    /**
     * 页面的初始数据
     */
    data: {
        scrollLeft: 0,
        TabCur: 2,
        menu: [{
                name: '未核销',
                english: 2
            },
            {
                name: '已核销',
                english: 3
            }
        ],
        orderDatail: {
            code: 123,
            code_qrcode: 1232132131
        },
        qrcode_w: qrcode_w,
        text: '',
        order: [{
                title: '商品标题',
                cover_img: 'https://cdn.jzenze.com/00642c435d05838a59e35a8cb7075890.jpeg',
                order_status: 1,
                order_status_desc: '商标迷哦奥数'
            },
            {
                title: '商品标题',
                cover_img: 'https://cdn.jzenze.com/00642c435d05838a59e35a8cb7075890.jpeg',
                order_status: 2,
                order_status_desc: '商标迷哦奥数'
            },
            {
                title: '商品标题',
                cover_img: 'https://cdn.jzenze.com/00642c435d05838a59e35a8cb7075890.jpeg',
                order_status: 3,
                order_status_desc: '商标迷哦奥数'
            }
        ]
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function(options) {
        this.getQuestionnaireItem()
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function() {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function() {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function() {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function() {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function() {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function() {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function() {

    },
    tabSelect(e) {
        this.setData({
            TabCur: e.currentTarget.dataset.id,
            scrollLeft: (e.currentTarget.dataset.id - 1) * 60
        })

    },
    getQuestionnaireItem () {
        Http.Post('api/Lottery/getPrizeList').then(res => {
            console.log(res)
        })
    },
    showModal(e) {
        this.setData({
            modalName: e.currentTarget.dataset.target,
            orderId: e.currentTarget.dataset.id
        })
        // Http.Post('api/Activity/getMyActivityDetail', {
        //     order_id: this.data.orderId
        // }).then(res => {
        //     if (res.code === 1000) {
        //         this.setData({
        //             orderDatail: res.data,
        //             text: res.data.delivery_code
        //         })
        //         var qrcode = new QRCode('canvas', {
        //             // usingIn: this,
        //             text: res.data.delivery_code,
        //             width: qrcode_w,
        //             height: qrcode_w,
        //             colorDark: "#000000",
        //             colorLight: "#ffffff",
        //             correctLevel: QRCode.CorrectLevel.H,
        //         });
        //     } else {
        //         wx.showToast({
        //             title: res.message,
        //         })
        //     }
        // })
    },
    hideModal(e) {
        this.setData({
            modalName: null
        })
    },
    confirmHandler: function(e) {
        var value = e.detail.value
        qrcode.makeCode(value)
    },
    inputHandler: function(e) {
        var value = e.detail.value
        this.setData({
            text: value
        })
    },
    tapHandler: function() {
        // 传入字符串生成qrcode
        qrcode.makeCode(this.data.text)
    },

})