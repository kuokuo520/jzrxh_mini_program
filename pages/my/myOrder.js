import Http from "../../utils/Api.js"
var QRCode = require('../../utils/weapp-qrcode.js')
const W = wx.getSystemInfoSync().windowWidth;
const rate = 750.0 / W;

// 300rpx 在6s上为 150px
const qrcode_w = 300 / rate;
Page({

    /**
     * 页面的初始数据
     */
    data: {
        scrollLeft: 0,
        TabCur: 0,
        menu: [{
                name: '全部',
                english: 0
            },
            {
                name: '待支付',
                english: 1
            }, {
                name: '未核销',
                english: 4
            },
            {
                name: '已核销',
                english: 6
            },
            {
                name: '退款',
                english: 13
            }
        ],
        orderDatail: {
            code: 123,
            code_qrcode: 1232132131
        },
        qrcode_w: qrcode_w,
        text: '',
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function(options) {
        this.getOrder()
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function() {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function() {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function() {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function() {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function() {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function() {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function() {

    },
    tabSelect(e) {
        this.setData({
            TabCur: e.currentTarget.dataset.id,
            scrollLeft: (e.currentTarget.dataset.id - 1) * 60
        })
        this.getOrder()
    },
    getOrder() {
        Http.Post('api/Order/getOrderList', {
            order_type: 3,
            order_status: this.data.TabCur
        }).then(res => {
            if (res.code === 1000) {
                this.setData({
                    order: res.data,
                    couponNull: false
                })
            } else {
                this.setData({
                    couponNull: true,
                    order: ''
                })
            }
        })
    },
    showModal(e) {
        console.log(e)
        this.setData({
            modalName: e.currentTarget.dataset.target,
            orderId: e.currentTarget.dataset.id,
            shopName: e.currentTarget.dataset.shop
        })
        Http.Post('api/Order/pickupCodeDetail', {
            order_id: this.data.orderId
        }).then(res => {
            if (res.code === 1000) {
                this.setData({
                    qrCode: res.data.qrcode_url,
                    text: res.data.delivery_code,
                    orderDetail: res.data
                })
            } else {
                wx.showToast({
                    title: res.message,
                })
            }
        })
    },
    hideModal(e) {
        this.setData({
            modalName: null
        })
    },
    confirmHandler: function(e) {
        var value = e.detail.value
        qrcode.makeCode(value)
    },
    inputHandler: function(e) {
        var value = e.detail.value
        this.setData({
            text: value
        })
    },
    tapHandler: function() {
        // 传入字符串生成qrcode
        qrcode.makeCode(this.data.text)
    },
    payOrder(e) {
        Http.Post('api/Seckill/continuePay', {
            order_id: e.currentTarget.dataset.id,
        }).then(res => {
            if (res.code === 1000) {
                wx.requestPayment({
                    timeStamp: res.data.jsapi.timeStamp,
                    nonceStr: res.data.jsapi.nonceStr,
                    package: res.data.jsapi.package,
                    signType: 'MD5',
                    paySign: res.data.jsapi.paySign,
                    success(res) {
                        wx.showToast({
                            title: res.message,
                        })
                        this.setData({
                            TabCur: 2
                        })
                    },
                    fail(res) {
                        wx.showToast({
                            title: '支付错误',
                            icon: 'none'
                        })
                    }
                })
            } else {
                wx.showModal({
                    title: '兑换失败',
                    content: res.message,
                    success(res) {
                        if (res.confirm) {
                            wx.navigateTo({
                                url: '/pages/my/myItem',
                            })
                        }
                    }
                })
            }
        })
    },
    cancelOrder(e) {
        let id = e.currentTarget.dataset.id
        wx.showModal({
            title: '',
            content: '是否取消订单',
            success: (res) => {
                if (res.confirm) {
                    Http.Post('api/Order/cancel', {
                        order_id: id
                    }).then(res => {
                        if (res.code === 1000) {
                            wx.showToast({
                                title: res.message,
                            })
                            this.getOrder()
                        }
                    })
                } else if (res.cancel) {
                    console.log('用户点击取消')
                }
            }
        })
    },
    refund(e) {
        wx.showModal({
            content: '是否要退款',
            success(res) {
                if (res.confirm) {
                    Http.Post('api/Order/refund', {
                        order_id: e.currentTarget.dataset.id
                    }).then(res => {
                        if (res.code === 1000) {
                            wx.showToast({
                                title: res.message,
                            })
                            this.getOrder()
                        }
                    })
                }
            }
        })
    }
})