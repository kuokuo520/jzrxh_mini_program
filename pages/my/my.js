var QRCode = require('../../utils/weapp-qrcode.js')
const W = wx.getSystemInfoSync().windowWidth;
const rate = 750.0 / W;
import Http from "../../utils/Api.js"
// 300rpx 在6s上为 150px
const qrcode_w = 300 / rate;
Page({

    /**
     * 页面的初始数据
     */
    data: {
        userInfo: {},
        text: '',
        codeImg: ''
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function(options) {
        this.authStatus()
        this.getUserInfo()
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function() {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function() {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function() {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function() {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function() {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function() {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function() {

    },
    scopCode() {
        wx.scanCode({
            onlyFromCamera: true,
            success(res) {
                console.log(res)
            }
        })
    },

    confirmHandler: function(e) {
        var value = e.detail.value
        qrcode.makeCode(value)
    },
    inputHandler: function(e) {
        var value = e.detail.value
        this.setData({
            text: value
        })
    },
    tapHandler: function() {
        // 传入字符串生成qrcode
        qrcode.makeCode(this.data.text)
    },
    getUserInfo(e) {
        Http.Post('api/user/userinfo').then(res => {
            if (res.code === -1004) {
                this.setData({
                    userInfo: res.data,
                    authStatus: false
                })
            } else {
                this.setData({
                    userInfo: res.data,
                    authStatus: true
                })
            }
        })
        this.setData({
            userInfo: e.detail,
            codeImg: e.detail.mobile
        })
        var qrcode = new QRCode('canvas', {
            // usingIn: this,
            text: e.detail.mobile,
            width: qrcode_w,
            height: qrcode_w,
            colorDark: "#000000",
            colorLight: "#ffffff",
            correctLevel: QRCode.CorrectLevel.H,
        });
    },
    activeQRcode() {
        wx.navigateTo({
            url: '/pages/my/myCode',
        })
    },
    hideModal(e) {
        this.setData({
            modalName: null
        })
    },
    scopCodeAcitive() {
        wx.scanCode({
            success(res) {
                Http.Post('api/WriteOff/writeOff', {
                    code: res.result
                }).then(res => {
                    if (res.code === 1000) {
                        wx.showModal({
                            title: '',
                            content: res.message,
                            showCancel: false
                        })
                        this.setData({
                            modalName: ''
                        })
                    } else {
                        wx.showModal({
                            title: '警告',
                            content: res.message,
                            showCancel: false
                        })
                    }
                })
            }
        })
    },
    activeModel() {
        wx.navigateTo({
            url: '/pages/admin/admin',
        })
        // this.setData({
        //     modalName: 'Image'
        // })
    },
    auth() {
        this.selectComponent("#auth").getUserInfo();
    },
    authStatus() {
        wx.getStorage({
            key: 'auth',
            success:(res) => {
                console.log(res)
                if (res.data === 'true') {
                    this.setData({
                        authStatus: true
                    })
                } else {
                    this.setData({
                        authStatus: false
                    })
                }
            }
        })
    }
})