import Http from '../../utils/Api.js'
import Time from '../../utils/time.js'
var WxParse = require('../../wxParse/wxParse.js');
Page({

    /**
     * 页面的初始数据
     */
    data: {
        user: {}
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function(options) {
        if (options.id !== undefined) {
            this.getCoupon(options.id)
        } else {
            this.getCoupon(options.scene)
        }
        this.getUserInfo()
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function() {
        this.shareActive = this.selectComponent("#shareActive")
    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function() {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function() {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function() {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function() {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function() {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function() {

    },
    submit() {
        this.setData({
            modalName: 'bottomModal'
        })
    },
    hideModal() {
        this.setData({
            modalName: ''
        })
    },
    getCoupon(id) {
        Http.Post('api/Goods/getGoodsDetail', {
            goods_id: id
        }).then(res => {
            if (res.code === 1000) {
                this.setData({
                    coupon: res.data,
                    startTime: Time.formatTime(res.data.sale_start_time, 'Y/M/D h:m:s'),
                    overTime: Time.formatTime(res.data.sale_end_time, 'Y/M/D h:m:s')
                })
				WxParse.wxParse('article', 'html', this.data.coupon.desc, this, 5);
            }
        })
        let query = id
        Http.Post('api/Wx/getPageQr', {
            query: query,
            page: `pages/coupon/couponPage`
        }).then(res => {
            if (res.code === 1000) {
                this.setData({
                    url: res.data.all_url
                })
            } else {
                wx.showToast({
                    title: res.message,
                    icon: 'none'
                })
            }
        })
    },
    buy(e) {
        Http.Post('api/Order/add', {
            goods_id: e.currentTarget.dataset.id,
            consignee: this.data.user.nickname,
            mobile: this.data.user.mobile
        }).then(res => {
            if (res.code === 1000) {
                wx.requestPayment({
                    timeStamp: res.data.jsapi.timeStamp,
                    nonceStr: res.data.jsapi.nonceStr,
                    package: res.data.jsapi.package,
                    signType: 'MD5',
                    paySign: res.data.jsapi.paySign,
                    success(res) {
                        wx.showToast({
                            title: '支付成功'
                        })
                        wx.navigateTo({
                            url: '/pages/my/myCoupon',
                        })
                    },
                    fail(res) {}
                })
            } else {
                wx.showToast({
                    title: res.message,
                })
            }
        })
    },
    getUserInfo() {
        Http.Post('api/user/userinfo').then(res => {
            if (res.code === 1000) {
                this.setData({
                    user: res.data
                })
            } else {
                wx.showModal({
                    title: '',
                    content: '需要获取您的用户才能体验所有功能，点击确定跳转登录。',
                    success(res) {
                        if (res.confirm) {
                            wx.switchTab({
                                url: '/pages/my/my',
                            })
                        } else if (res.cancel) {

                        }
                    }
                })
            }
        })
    },
    shareStatus() {
        this.shareActive.shareActive()
    },
    imgLoad(e) {
        this.setData({
            iWidth: e.detail.width,
            iHeight: e.detail.height
        })
    },
    formSubmit(e) {
        let openid = ''
        wx.getStorage({
            key: 'openid',
            success: (res) => {
                openid = res.data
            },
        })
        setTimeout(() => {
            Http.Post('api/Wx/saveFormId', {
                openid: openid,
                formId: e.detail.formId
            }).then(res => {
                console.log(res)
                this.postCoupon()
            })
        }, 500)
    },
    postCoupon() {
        Http.Post('/api/Marketing/pushGoods').then(res => {
            console.log(res)
        })
    }
})