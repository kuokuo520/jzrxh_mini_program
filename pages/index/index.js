import Http from '../../utils/Api.js'
Page({

    /**
     * 页面的初始数据
     */
    data: {
        adList: [],
        drList: [],
        bannerImageHeight: '',
        questStatus: false,
        paperStatus: false
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        this.getBanner()
        this.getActiveQuest()
        this.getPaper(1)
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {
    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {
		this.selectComponent("#groupTemplate").getCoupon()
    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    },
    getBanner () {
        Http.Post('api/Ad/getAdList', { position_code: 'AvVfF1Wm'}).then(res => {
            this.setData({
                adList: res.data
            })
        })
        Http.Post('api/Ad/getAdList', { position_code: 'drwre3' }).then(res => {
            this.setData({
                drList: res.data
            })
        })
    },
    imageLoad(e) {
        this.setData({
            bannerImageHeight: e.detail.height
        })
    },
    activeQuest () {
        wx.setStorage({
            key: "quest",
            data: "true"
        })
        wx.navigateTo({
            url: '/pages/questionnaire/questionnaire',
        })
    },
    getPaper(id) {
        Http.Post('api/Survey/getPaper', { paper_id: id}).then(res => {
            if (res.code === 1000) {
                this.setData({
                    paperStatus: true
                })
            }
        })
    },
    getActiveQuest () {
        wx.getStorage({
            key: 'quest',
            success: (res) => {
                console.log(res)
                this.setData({
                    questStatus: res.data
                })
            }
        })
    },
    closeQuest () {
        wx.setStorage({
            key: "quest",
            data: "true"
        })
        this.getActiveQuest()
    }
})