import Http from '../../utils/Api.js'
Page({

    /**
     * 页面的初始数据
     */
    data: {
        modalName: 'DialogModal1',
        line: [],
        id: '',
        shop: {}
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function(options) {
        this.setData({
            id: options.id
        })
        this.getQuenue(options.id)
        this.getShop(this.data.id)
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function() {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function() {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function() {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function() {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function() {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function() {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function() {

    },
    upNumber() {
        this.setData({
            numberModel: true,
            animated: 'animated fadeOut'
        })
    },
    getQuenue() {
        Http.Post('api/Quenue/getQuenueByTable', {
            shop_id: this.data.id
        }).then(res => {
            if (res.code === 1000) {
                this.setData({
                    line: res.data
                })
                wx.showLoading({
                    title: '加载中',
                })
                setTimeout(function() {
                    wx.hideLoading()
                }, 1000)
            } else {
                wx.showToast({
                    title: '暂无客人排队',
                    icon: 'none'
                })
            }
        })
    },
    getShop(id) {
        Http.Post('api/Shop/shopDetail', {
            shop_id: this.data.id
        }).then(res => {
            if (res.code === 1000) {
                this.setData({
                    shop: res.data
                })
            } else if (res.code === -1004) {
                wx.showModal({
                    title: '',
                    content: '需要获取您的用户才能体验所有功能，点击确定跳转登录。',
                    success(res) {
                        if (res.confirm) {
                            wx.switchTab({
                                url: '/pages/my/my',
                            })
                        } else if (res.cancel) {

                        }
                    }
                })
            }
        })
    },
    renovate() {
        this.getQuenue()
    }
})