import Http from '../../utils/Api.js'
Page({

    /**
     * 页面的初始数据
     */
    data: {
        detail: {}
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function(options) {
        this.getLine(options.id)
        this.setData({id: options.id})
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function() {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function() {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function() {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function() {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function() {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function() {

    },
    onUnload: function () {
        wx.reLaunch({
            url: '/pages/index/index'
        })
    },
    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function() {

    },
    close() {
        this.setData({ modalName: 'bottomModal'})
    },
    hideModal () {
        this.setData({ modalName: '' })
    },
    getLine (id) {
        Http.Post('api/Quenue/getDetail', { shop_id: id, page: 1}).then(res => {
            if (res.code === 1000) {
                this.setData({detail: res.data})
                wx.showLoading({
                    title: '加载中',
                })
                setTimeout(function () {
                    wx.hideLoading()
                }, 1000)
            }
        })
    },
    renovate() {
        this.getLine(this.data.id)
    },
    cancel () {
        Http.Post('api/Quenue/cancel', { queue_id: this.data.detail.info.queue_id}).then(res => {
            if (res.code === 1000) {
                wx.showToast({
                    title: res.message,
                })
                setTimeout(function () {
                    wx.switchTab({
                        url: '/pages/index/index',
                    })
                }, 1000)
            } else {
                wx.showToast({
                    title: res.message,
                    icon: 'none'
                })
            }
        })
    }
})