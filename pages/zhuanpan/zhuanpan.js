import Http from '../../utils/Api.js'
let interval1, interval2;
Page({
    data: {
        text: '这是一行文字水平滚动效果，在小程序中实现的', //滚动文字
        duration: 0, //水平滚动方法一中文字滚动总时间
        pace: 0.5, //滚动速度
        posLeft1: 0, //水平滚动方法二中left值
        posLeft2: 0, //水平滚动方法三中left值
        marginLeft: 100, //水平滚动方法三中两条文本之间的间距
        size: 600, //转盘大小,
        musicflg: false, //声音
        fastJuedin: false, //快速决定
        repeat: false, //不重复抽取
        probability: false, // 概率
        s_awards: '', //结果
        option: '标题',
        status: '',
        lotteryData: {},
        startLottery: [],
        //转盘的总数据，想添加多个可以往这数组里添加一条格式一样的数据
        zhuanpanArr: [{
            id: 0,
            option: '我帅吗？', //转盘的标题名称
            awards: [{
                    id: 0, // id递增
                    name: "帅", // 选项名 超过9个字时字体会变小点 大于13个数时会隐藏掉超出的
                    color: '#fce8ba', // 选项的背景颜色
                    probability: 0 // 概率 0代表永远也转不到这个选项，数字越大概率也就越大,data中的probability属性设置为true时是才生效, 这属性也必须填写，不填写会出错
                },
                {
                    id: 1,
                    name: "很帅",
                    color: '#fbdd9d',
                    probability: 10
                },
                {
                    id: 2,
                    name: "贼帅",
                    color: '#fce8ba',
                    probability: 10
                },
                {
                    id: 3,
                    name: "非常帅",
                    color: '#fbdd9d',
                    probability: 10
                },
                {
                    id: 4,
                    name: "超级帅",
                    color: '#fce8ba',
                    probability: 60
                },
                {
                    id: 4,
                    name: "宇宙无敌第一帅",
                    color: '#fbdd9d',
                    probability: 10
                }
            ]
        }, ],
        //更改数据可以更改这属性，格式要像下面这样写才行
        awardsConfig: {
            option: '我帅吗？', //转盘的标题名称
            awards: [{
                    id: 0, // id递增
                    name: "帅", // 选项名 超过9个字时字体会变小点 大于13个数时会隐藏掉超出的
                    color: '#fce8ba', // 选项的背景颜色
                    probability: 0 // 概率 0代表永远也转不到这个选项，数字越大概率也就越大,data中的probability属性设置为true时是才生效, 这属性也必须填写，不填写会出错
                },
                {
                    id: 1,
                    name: "很帅",
                    color: '#fbdd9d',
                    probability: 0
                },
                {
                    id: 2,
                    name: "贼帅",
                    color: '#fce8ba',
                    probability: 0
                },
                {
                    id: 3,
                    name: "非常帅",
                    color: '#fbdd9d',
                    probability: 0
                },
                {
                    id: 4,
                    name: "超级帅",
                    color: '#fce8ba',
                    probability: 0
                },
                {
                    id: 5,
                    name: "宇宙无敌第一帅",
                    color: '#fbdd9d',
                    probability: 0
                }
            ]
        },

        //接收当前转盘初始化时传来的参数
        getData(e) {
            this.setData({
                option: e.detail.option
            })
        },

        //接收当前转盘结束后的答案选项
        getAwards(e) {
            console.log(e)
            wx.showToast({
                title: e.detail,
                icon: 'none'
            })
            this.setData({
                s_awards: e.detail,
            })
        },

        //开始转动或者结束转动
        startZhuan(e) {
            this.setData({
                zhuanflg: e.detail ? true : false
            })
        },

        //切换转盘选项
        switchZhuanpan(e) {
            //当转盘停止时才执行切换转盘
            if (!this.data.zhuanflg) {
                var idx = e.currentTarget.dataset.idx,
                    zhuanpanArr = this.data.zhuanpanArr,
                    obj = {};
                for (let i in zhuanpanArr) {
                    if (this.data.option != zhuanpanArr[i].option && zhuanpanArr[i].id == idx) {
                        obj.option = zhuanpanArr[i].option;
                        obj.awards = zhuanpanArr[i].awards;
                        this.setData({
                            awardsConfig: obj //其实默认要更改当前转盘的数据要传个这个对象，才有效果
                        })
                        break;
                    }
                }
            }
        },

        //转盘声音
        switch1Change1(e) {
            var value = e.detail.value;
            if (this.data.zhuanflg) {
                wx.showToast({
                    title: '当转盘停止转动后才有效',
                    icon: 'none'
                })
                return;
            } else {
                this.setData({
                    musicflg: value
                })
            }
        },

        //不重复抽取
        switch1Change2(e) {
            var value = e.detail.value;
            if (this.data.zhuanflg) {
                wx.showToast({
                    title: '当转盘停止转动后才有效',
                    icon: 'none'
                })
                return;
            } else {
                this.setData({
                    repeat: value
                })
            }
        },

        //快速决定
        switch1Change3(e) {
            var value = e.detail.value;
            if (this.data.zhuanflg) {
                wx.showToast({
                    title: '当转盘停止转动后才有效',
                    icon: 'none'
                })
                return;
            } else {
                this.setData({
                    fastJuedin: value
                })
            }
        },

        //概率 == 如果不重复抽取开启的话 概率是无效的
        switch1Change4(e) {
            var value = e.detail.value;
            if (this.data.zhuanflg) {
                wx.showToast({
                    title: '当转盘停止转动后才有效',
                    icon: 'none'
                })
                return;
            } else {
                this.setData({
                    probability: true
                })
            }
        },

        onLoad: function() {
            this.getData()
            //实例化组件对象，这样有需要时就能调用组件内的方法
            this.zhuanpan = this.selectComponent("#zhuanpan");

            //可以这样调用 示例：this.zhuanpan.switchZhuanpan(data); 
            //上面这方法可用来切换转盘选项数据，参数可以看组件构造器中的switchZhuanpan方法
        }
    },
    roll1: function(that, txtLength, windowWidth) {
        interval1 = setInterval(function() {
            if (-that.data.posLeft1 < txtLength) {
                that.setData({
                    posLeft1: that.data.posLeft1 - that.data.pace
                })
            } else {
                that.setData({
                    posLeft1: windowWidth
                })
            }
        }, 20)
    },
    roll2: function(that, txtLength, windowWidth) {
        interval2 = setInterval(function() {
            if (-that.data.posLeft2 < txtLength + that.data.marginLeft) {
                that.setData({
                    posLeft2: that.data.posLeft2 - that.data.pace
                })
            } else { // 第二段文字滚动到左边后重新滚动
                that.setData({
                    posLeft2: 0
                })
                clearInterval(interval2);
                that.roll2(that, txtLength, windowWidth);
            }
        }, 20)
    },
    onShow: function() {
        this.getMyLottery()
        this.getLottery()
        this.getZhuanPan()
        let that = this;
        let windowWidth = wx.getSystemInfoSync().windowWidth; //屏幕宽度
        wx.createSelectorQuery().select('#txt1').boundingClientRect(function(rect) {
            let duration = rect.width * 0.03; //滚动文字时间,滚动速度为0.03s/px
            that.setData({
                duration: duration
            })
        }).exec()

        wx.createSelectorQuery().select('#txt2').boundingClientRect(function(rect) {
            let txtLength = rect.width; //滚动文字长度
            that.roll1(that, txtLength, windowWidth);
        }).exec()

        wx.createSelectorQuery().select('#txt3').boundingClientRect(function(rect) {
            let txtLength = rect.width; //文字+图标长度
            that.setData({
                marginLeft: txtLength < windowWidth - that.data.marginLeft ? windowWidth - txtLength : that.data.marginLeft
            })
            that.roll2(that, txtLength, windowWidth);
        }).exec()
    },
    onHide: function() {
        clearInterval(interval1);
        clearInterval(interval2);
    },
    submit(e) {
        if (e.detail === "prize") {
            this.setData({
                status: e.detail
            })
        } else {
            this.setData({
                status: e.currentTarget.dataset.name
            })
        }
        this.getMyLottery()
    },
    getLottery() {
        Http.Post('api/Lottery/getPrizeList', {}).then(res => {
            if (res.code === -1004) {
                wx.showModal({
                    title: '',
                    content: '需要获取您的用户才能体验所有功能，点击确定跳转登录。',
                    success(res) {
                        if (res.confirm) {
                            wx.switchTab({
                                url: '/pages/my/my',
                            })
                        } else if (res.cancel) {

                        }
                    }
                })
            }
            if (res.code === 1000) {
                this.setData({
                    startLottery: res.data
                })
            }
        })
    },
    getData() {
        Http.Post('api/Lottery/getData', {}).then(res => {
            if (res.code === 1000) {
                this.setData({
                    lotteryData: res.data
                })
            }
        })
        this.getMyLottery()
    },
    getMyLottery() {
        Http.Post('api/Lottery/getLotteryRecord', {
            isMe: 1
        }).then(res => {
            if (res.code === 1000) {
                this.setData({
                    myLottery: res.data
                })
            }
        })
    },
})