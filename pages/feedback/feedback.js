import Http from '../../utils/Api.js'
Page({

    /**
     * 页面的初始数据
     */
    data: {
        userSex: 1
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function(options) {

    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function() {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function() {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function() {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function() {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function() {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function() {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function() {

    },
    putName(e) {
        let data = e.detail.value
        this.setData({
            userName: data
        })
    },
    putPhone(e) {
        let data = e.detail.value
        this.setData({
            userPhone: data
        })
    },
    textareaBInput(e) {
        let data = e.detail.value
        this.setData({
            content: data
        })
    },
    switch1Change(e) {
        let data = e.detail.value
        if (data === true) {
            data = 1
        } else {
            data = 2
        }
        this.setData({
            userSex: data
        })
    },
    enlist() {
        Http.Post('api/Opinion/add', {
            content: this.data.content,
            user_name: this.data.userName,
            user_tel: this.data.userPhone,
            user_sex: this.data.userSex
        }).then(res => {
            if (res.code === 1000) {
                wx.showToast({
                    title: res.message
                })
                setTimeout(function() {
                    wx.switchTab({
                        url: '/pages/index/index',
                    })
                }, 1000);
            } else {
                wx.showModal({
                    title: '',
                    content: '需要获取您的用户才能体验所有功能，点击确定跳转登录。',
                    success(res) {
                        if (res.confirm) {
                            wx.switchTab({
                                url: '/pages/my/my',
                            })
                        } else if (res.cancel) {

                        }
                    }
                })
                wx.showToast({
                    title: res.message,
                    icon: 'none'
                })
            }
        })
    }
})