import Http from '../../utils/Api.js'
const arr = []
Page({

    /**
     * 页面的初始数据
     */
    data: {
        classId: 0,
        animationData: {},
        floors: [],
        classify: [],
        shopList: {},
        floor_id: null,
        classify_id: null,
        size: 1,
        isLoad: true
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function(options) {
        this.getFloors()
        this.getClassify()
        this.getShopList()
        this.getBanner()
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function() {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function() {
        const animation = wx.createAnimation({
            duration: 1000,
            timingFunction: 'ease',
        })
    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function() {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function() {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function() {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function() {
        this.getSize()
    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function() {

    },
    activeMenu(e) {
        let id = e.currentTarget.dataset.id
        if (id === this.data.classId) {
            this.setData({
                classId: 0
            })
        } else {
            this.setData({
                classId: id
            })
        }
    },
    getFloors() {
        Http.Post('api/Mall/getFloors').then(res => {
            if (res.code === 1000) {
                this.setData({
                    floors: res.data
                })
            }
        })
    },
    getClassify() {
        Http.Post('api/Shop/getClassify').then(res => {
            if (res.code === 1000) {
                this.setData({
                    classify: res.data
                })
            }
        })
    },
    getShopList(name) {
        if (name === undefined) {
            Http.Post('api/Shop/shopList').then(res => {
                if (res.code === 1000) {
                    this.setData({
                        shopList: res.data,
                        shopSize: res.allPage
                    })
                }
            })
        } else {
            if (this.data.classify_id === '') {
                Http.Post('api/Shop/shopList', {
                    floor_id: this.data.floor_id,
                }).then(res => {
                    if (res.code === 1000) {
                        this.setData({
                            shopList: res.data
                        })
                    } else if (res.code === -1000) {
                        this.setData({
                            shopList: '',
                            isLoad: false
                        })
                        wx.showToast({
                            title: res.message,
                            icon: 'none'
                        })
                    }
                })
            } else if (this.data.floor_id === '') {
                Http.Post('api/Shop/shopList', {
                    classify_id: this.data.classify_id,
                }).then(res => {
                    if (res.code === 1000) {
                        this.setData({
                            shopList: res.data
                        })
                    } else if (res.code === -1000) {
                        this.setData({
                            shopList: '',
                            isLoad: false
                        })
                        wx.showToast({
                            title: res.message,
                            icon: 'none'
                        })
                    }
                })
            } else {
                Http.Post('api/Shop/shopList', {
                    classify_id: this.data.classify_id,
                    floor_id: this.data.floor_id,
                }).then(res => {
                    if (res.code === 1000) {
                        this.setData({
                            shopList: res.data
                        })
                    } else if (res.code === -1000) {
                        this.setData({
                            shopList: '',
                            isLoad: false
                        })
                        wx.showToast({
                            title: res.message,
                            icon: 'none'
                        })
                    }
                })
            }
        }
    },
    getSize() {
        if (this.data.shopSize > this.data.size) {
            if (this.data.size === 1) {
                this.data.shopList.forEach((item => {
                    arr.push(item)
                }))
            }
            this.data.size++
                Http.Post('api/Shop/shopList', {
                    floor_id: this.data.floor_id,
                    classify_id: this.data.classify_id,
                    page: this.data.size
                }).then(res => {
                    if (res.code === 1000) {
                        res.data.forEach((item => {
                            arr.push(item)
                        }))
                        this.setData({
                            shopList: arr
                        })
                    } else if (res.code === -1000) {
                        this.setData({
                            isLoad: false
                        })
                    }
                })
        } else {
            this.setData({
                isLoad: false
            })
        }
    },
    activeFloors(e) {
        let name = e.currentTarget.dataset.name
        this.setData({
            floor_id: e.currentTarget.dataset.id,
            classId: '',
            size: 1
        })
        arr.length = 0;
        this.getShopList(name)
    },
    activeClassify(e) {
        let name = e.currentTarget.dataset.name
        this.setData({
            classify_id: e.currentTarget.dataset.id,
            classId: ''
        })
        arr.length = 0;
        this.getShopList(name)
    },
    getBanner() {
        Http.Post('api/Ad/getAdList', {
            position_code: 're235v'
        }).then(res => {
            this.setData({
                adList: res.data
            })
        })
    },
    searchShopList(e) {
        if (e.detail.length === '') {
            this.getShopList()
        } else {
            this.setData({
                shopList: e.detail
            })
        }
    }
})