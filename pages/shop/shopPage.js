import Http from '../../utils/Api.js'
var WxParse = require('../../wxParse/wxParse.js');
import Time from '../../utils/time.js'
Page({

    /**
     * 页面的初始数据
     */
    data: {
        shop: {},
        good: []
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        this.getShopInfo(options.id)
        this.getGoodList(options.id)
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    },
    getShopInfo(e) {
        Http.Post('api/Shop/shopDetail', { shop_id: e}).then(res => {
            if (res.code === 1000) {
                this.setData({
                    shop: res.data
                })
                WxParse.wxParse('article', 'html', this.data.shop.desc, this, 5);
            }
        })
    },
    getGoodList(id) {
        Http.Post('api/Seckill/getGoodsList', { shop_id: id}).then(res => {
            if (res.code === 1000) {
                this.setData({
                    good: res.data
                })
				var date = Date.parse(new Date()) / 1000
				this.setData({
					date: date
				})
            }
        })
    },
    navPaidui () {
        wx.navigateTo({
            url: `/pages/line/line?id=${this.data.shop.shop_id}`,
        })
    }
})