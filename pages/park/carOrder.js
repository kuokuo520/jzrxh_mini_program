import Http from '../../utils/Api.js'
Page({

    /**
     * 页面的初始数据
     */
    data: {
        coupon: [],
        couponId: null,
        current: '',
        buyStatus: false
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function(options) {
        this.queryPrice(options.car)
        this.getCouponLists(options.car)
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function() {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function() {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function() {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function() {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function() {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function() {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function() {

    },
    queryPrice(car) {
        Http.Post('api/Park/queryPrice', {
            carnum: car
        }).then(res => {
            if (res.code === 1000) {
                this.setData({
                    queryPrice: res.data,
                    total: res.data.total,
                    car: car
                })
            } else {
                wx.showModal({
                    title: '',
                    content: '未查询到车辆数据',
                    showCancel: false,
                    success(res) {
                        if (res.confirm) {
                            wx.navigateBack({
                                delta: 1
                            })
                        }
                    }
                })
            }
        })
    },
    getCouponLists(car) {
        Http.Post('api/Park/getCouponLists', {
            carnum: car
        }).then(res => {
            if (res.code === 1000) {
                this.setData({
                    coupon: res.data
                })
            }
        })
    },
    PickerChange(e) {
        this.setData({
            current: e.detail.value,
            couponId: this.data.coupon[e.detail.value].coupon_id
        })
        let total = this.data.queryPrice.total - this.data.coupon[this.data.current].money
        this.setData({
            total: total
        })
    },
    bindcancel() {
        this.setData({
            current: '',
            couponId: null
        })
    },
    addOrder() {
        this.setData({
            buyStatus: true
        })
        Http.Post('api/Park/addOrder', {
            carnum: this.data.car,
            coupon_id: this.data.couponId
        }).then(res => {
            if (res.code === 1000) {
                wx.requestPayment({
                    timeStamp: res.data.jsapi.timeStamp,
                    nonceStr: res.data.jsapi.nonceStr,
                    package: res.data.jsapi.package,
                    signType: 'MD5',
                    paySign: res.data.jsapi.paySign,
                    success: (res) => {
                        wx.showModal({
                            title: '',
                            content: res.message,
                            showCancel: false,
                            success(res) {
                                if (res.confirm) {
                                    wx.navigateBack({
                                        delta: 1
                                    })
                                }
                            }
                        })
                        this.setData({
                            buyStatus: false
                        })
                    },
                    fail(res) {
                        wx.showTabBar({
                            message: res.message
                        })
                    }
                })
            }
        })
    }
})