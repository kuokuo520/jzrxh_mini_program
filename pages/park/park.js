import Http from '../../utils/Api.js'
Page({

    /**
     * 页面的初始数据
     */
    data: {
        park: {},
        myCar: {},
        queryPrice: {},
        arr: []
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function(options) {
        this.getPark()
        this.getMyCarLists()
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function() {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function() {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function() {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function() {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function() {
        wx.showNavigationBarLoading() //在标题栏中显示加载
        this.onLoad()
        setTimeout(function() {
            wx.hideNavigationBarLoading() //完成停止加载
            wx.stopPullDownRefresh() //停止下拉刷新
        }, 1500);
    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function() {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function() {

    },
    telPhone() {
        wx.makePhoneCall({
            phoneNumber: '07168880033' // 仅为示例，并非真实的电话号码
        })
    },
    getPark() {
        Http.Post('api/Park/getParkingLists').then(res => {
            this.setData({
                park: res.data
            })
        })
    },
    getMyCarLists() {
        Http.Post('api/Park/getMyCarLists').then(res => {
            if (res.code === -1004) {
                wx.showModal({
                    title: '',
                    content: '需要获取您的用户才能体验所有功能，点击确定跳转登录。',
                    success(res) {
                        if (res.confirm) {
                            wx.switchTab({
                                url: '/pages/my/my',
                            })
                        } else if (res.cancel) {

                        }
                    }
                })
            }
            this.setData({
                myCar: res.data
            })
            var carName = []
            this.data.myCar.forEach((item) => {
                let name = item.car_number.substring(0, 1)
                carName.push(name)
            })
            this.setData({
                carName: carName
            })
            this.data.arr = []
            res.data.forEach((item, index) => {
                this.queryPrice(item.car_number)
            })
        })
    },
    queryPrice(id) {
        Http.Post('api/Park/queryPrice', {
            carnum: id
        }).then(res => {
            this.data.arr.push(res)
            this.data.arr = this.data.arr.reverse()
			this.setData({
                queryPrice: this.data.arr
			})
        })
    },
    // ListTouch触摸开始
    ListTouchStart(e) {
        this.setData({
            ListTouchStart: e.touches[0].pageX
        })
    },

    // ListTouch计算方向
    ListTouchMove(e) {
        this.setData({
            ListTouchDirection: e.touches[0].pageX - this.data.ListTouchStart > 0 ? 'right' : 'left'
        })
    },

    // ListTouch计算滚动
    ListTouchEnd(e) {
        if (this.data.ListTouchDirection == 'left') {
            this.setData({
                modalName: e.currentTarget.dataset.target
            })
        } else {
            this.setData({
                modalName: null
            })
        }
        this.setData({
            ListTouchDirection: null
        })
    },
    deleteCar(id) {
        Http.Post('api/Park/delCar', {
            car_number: id.currentTarget.dataset.id
        }).then(res => {
            if (res.code === 1000) {
                wx.showToast({
                    title: res.message
                })
                this.onLoad()
            }
        })
    }
})