import Http from '../../utils/Api.js'
Page({

    /**
     * 页面的初始数据
     */
    data: {
        carNumber: '',
        city: [{
                id: 1,
                name: '京'
            },
            {
                id: 2,
                name: '津'
            }, {
                id: 3,
                name: '冀'
            }, {
                id: 4,
                name: '晋'
            }, {
                id: 5,
                name: '蒙'
            }, {
                id: 6,
                name: '吉'
            }, {
                id: 7,
                name: '粤'
            }, {
                id: 8,
                name: '鲁'
            }, {
                id: 9,
                name: '甘'
            }, {
                id: 10,
                name: '川'
            }, {
                id: 11,
                name: '辽'
            }, {
                id: 12,
                name: '黑'
            }, {
                id: 13,
                name: '泸'
            }, {
                id: 14,
                name: '苏'
            }, {
                id: 15,
                name: '宁'
            }, {
                id: 16,
                name: '豫'
            }, {
                id: 17,
                name: '鄂'
            }, {
                id: 18,
                name: '湘'
            }, {
                id: 19,
                name: '琼'
            }, {
                id: 20,
                name: '陕'
            }, {
                id: 21,
                name: '藏'
            }, {
                id: 22,
                name: '浙'
            }, {
                id: 23,
                name: '皖'
            }, {
                id: 24,
                name: '闽'
            }, {
                id: 25,
                name: '赣'
            }, {
                id: 26,
                name: '桂'
            }, {
                id: 27,
                name: '云'
            }, {
                id: 28,
                name: '贵'
            }, {
                id: 29,
                name: '渝'
            }, {
                id: 30,
                name: '青'
            }, {
                id: 31,
                name: '新'
            },
        ],
        number: [{
            id: 1,
            name: 0
        }, {
            id: 2,
            name: 1
        }, {
            id: 3,
            name: 2
        }, {
            id: 4,
            name: 3
        }, {
            id: 5,
            name: 4
        }, {
            id: 6,
            name: 5
        }, {
            id: 7,
            name: 6
        }, {
            id: 8,
            name: 7
        }, {
            id: 9,
            name: 8
        }, {
            id: 10,
            name: 9
        }, {
            id: 11,
            name: 'A'
        }, {
            id: 12,
            name: 'B'
        }, {
            id: 13,
            name: 'C'
        }, {
            id: 14,
            name: 'D'
        }, {
            id: 15,
            name: 'E'
        }, {
            id: 16,
            name: 'F'
        }, {
            id: 18,
            name: 'G'
        }, {
            id: 19,
            name: 'H'
        }, {
            id: 20,
            name: 'J'
        }, {
            id: 21,
            name: 'K'
        }, {
            id: 22,
            name: 'L'
        }, {
            id: 23,
            name: 'M'
        }, {
            id: 24,
            name: 'N'
        }, {
            id: 25,
            name: 'P'
        }, {
            id: 26,
            name: 'Q'
        }, {
            id: 27,
            name: 'R'
        }, {
            id: 28,
            name: 'S'
        }, {
            id: 29,
            name: 'T'
        }, {
            id: 30,
            name: 'U'
        }, {
            id: 31,
            name: 'V'
        }, {
            id: 32,
            name: 'W'
        }, {
            id: 33,
            name: 'X'
        }, {
            id: 34,
            name: 'Y'
        }, {
            id: 35,
            name: 'Z'
        }, ],
        cityText: '',
        numberId: '1',
        plateNumber1: '',
        plateNumber2: '',
        plateNumber3: '',
        plateNumber4: '',
        plateNumber5: '',
        plateNumber6: '',
        plateNumber7: '',
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function(options) {

    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function() {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function() {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function() {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function() {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function() {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function() {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function() {

    },
    activeCity(e) {
        var name = e.currentTarget.dataset.name
        switch (parseInt(this.data.numberId)) {
            case 1:
                this.setData({
                    cityText: name
                })
                break;
            case 2:
                this.setData({
                    plateNumber1: name
                })
                break;
            case 3:
                this.setData({
                    plateNumber2: name
                })
                break;
            case 4:
                this.setData({
                    plateNumber3: name
                })
                break;
            case 5:
                this.setData({
                    plateNumber4: name
                })
                break;
            case 6:
                this.setData({
                    plateNumber5: name
                })
                break;
            case 7:
                this.setData({
                    plateNumber6: name
                })
                break;
            case 8:
                this.setData({
                    plateNumber7: name
                })
                break;
        }
        let carNumber = [this.data.cityText, this.data.plateNumber1, this.data.plateNumber2, this.data.plateNumber3, this.data.plateNumber4, this.data.plateNumber5, this.data.plateNumber6, this.data.plateNumber7]
        let data = carNumber.join('')
        this.setData({ carNumber: data})
        if (this.data.numberId < 7) {
            let number = parseInt(this.data.numberId)
            number++
            this.setData({
                numberId: number
            })
        }
    },
    activeNumber(e) {
        this.setData({
            numberId: e.currentTarget.dataset.id
        })
    },
    addCar() {
        Http.Post('api/Park/addCar', {
            car_number: this.data.carNumber
        }).then(res => {
            if (res.code === 1000) {
                wx.showModal({
                    title: '',
                    content: res.message,
                    showCancel: false,
                    success: function(res) {
                        if (res.confirm) {
                            wx. navigateTo({
                                url: '/pages/park/park'
                            })
                        }
                    },
                })
            } else {
                wx.showModal({
                    title: '',
                    content: res.message,
                    showCancel: false
                })
            }
        })
    }
})