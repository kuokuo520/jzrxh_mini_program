import Http from '../../utils/Api.js'
import Time from '../../utils/time.js'
Page({

    /**
     * 页面的初始数据
     */
    data: {
        shopId: '',
        list: {},
        startTime: []
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function(options) {
        this.getUserInfo()
        setInterval(()=> {
            this.getCallList();
        }, 30000)    //代表1秒钟发送一次请求
        wx.hideShareMenu()
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function() {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function() {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function() {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function() {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function() {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function() {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function() {

    },
    getCallList() {
        Http.Post('api/Quenue/getCallList', {
            shop_id: this.data.shopId
        }).then(res => {
            if (res.code === 1000) {
                this.setData({
                    list: res.data
                })
                var countDownArr = [];
                var startTime = [];
                this.data.list.list.forEach((item, i) => {
                    var actEndTime = item.book_time
                    var timestamp = Date.parse(new Date());
                    timestamp = timestamp / 1000
                    if (timestamp - actEndTime > 0) {
                        let time = timestamp - actEndTime;
                        // 获取天、时、分、秒
                        let hou = parseInt(time % (60 * 60 * 24) / 3600);
                        let min = parseInt(time % (60 * 60 * 24) % 3600 / 60);
                        var obj = {
                            hou: hou,
                            min: this.timeFormat(min),
                        }
                    } else { //活动已结束，全部设置为'00'
                        var obj = {
                            hou: '00',
                            min: '00',
                        }
                    }
                    startTime.push(obj);
                })
                this.setData({
                    startTime: startTime
                })
            } else {
                if (res.code === -1003) {
                    this.setData({
                        list: ''
                    })
                }
            }
        })
    },
    timeFormat(param) { //小于10的格式化函数
        return param < 10 ? '0' + param : param;
    },
    getUserInfo() {
        Http.Post('api/user/userinfo').then(res => {
            if (res.code === 1000) {
                this.setData({
                    shopId: res.data.shop_id
                })
                this.getCallList()
                this.getShop(res.data.shop_id)
            }
        })
    },
    call(e) {
        let id = e.currentTarget.dataset.id
        Http.Post('api/Quenue/call', {
            queue_id: id,
            shop_id: this.data.shopId
        }).then(res => {
            if (res.code === 1000) {
                wx.showToast({
                    title: res.message,
                })
                this.getCallList()
            }
        })
    },
    dining(e) {
        let id = e.currentTarget.dataset.id
        Http.Post('api/Quenue/dining', {
            queue_id: id,
            shop_id: this.data.shopId
        }).then(res => {
            if (res.code === 1000) {
                wx.showToast({
                    title: res.message,
                })
                this.getCallList()
            }
        })
    },
    discard(e) {
        let id = e.currentTarget.dataset.id
        Http.Post('api/Quenue/discard', {
            queue_id: id,
            shop_id: this.data.shopId
        }).then(res => {
            if (res.code === 1000) {
                wx.showToast({
                    title: res.message,
                })
                this.getCallList()
            }
        })
    },
    getShop(id) {
        Http.Post('api/Shop/shopDetail', { shop_id: id}).then(res => {
            this.setData({ shopTitle: res.data.name})
        })
    }
})