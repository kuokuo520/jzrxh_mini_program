import Http from '../../utils/Api.js'
import Time from '../../utils/time.js'
var WxParse = require('../../wxParse/wxParse.js');
Page({

    /**
     * 页面的初始数据
     */
    data: {
        good: [],
        time: {},
        status: 'over',
        bannerHeight: ''
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function(options) {
        if (options.id !== undefined) {
            this.getSeckill(options.id)
        } else {
            this.getSeckill(options.scene)
        }
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function() {
        this.shareActive = this.selectComponent("#shareActive")
    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function() {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function() {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function() {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function() {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function() {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function() {

    },
    submit(e) {
        wx.navigateTo({
            url: `/pages/seckill/enlist?id=${e.currentTarget.dataset.id}`,
        })
    },
    timeFormat(param) { //小于10的格式化函数
        return param < 10 ? '0' + param : param;
    },
    getSeckill(e) {
        Http.Post('api/Seckill/getGoodsDetail', {
            goods_id: e
        }).then(res => {
            if (res.code === -1004) {
                wx.showModal({
                    title: '',
                    content: '需要获取您的用户才能体验所有功能，点击确定跳转登录。',
                    success(res) {
                        if (res.confirm) {
                            wx.switchTab({
                                url: '/pages/my/my',
                            })
                        } else if (res.cancel) {

                        }
                    }
                })
            }
            if (res.code === 1000) {
                this.setData({
                    good: res.data
                })
                WxParse.wxParse('article', 'html', res.data.desc, this, 5);
                wx.getImageInfo({
                    src: res.data.banner[0],
                    success: (res) => {
                        this.setData({ bannerHeight: res.height })
                    }
                })
                this.getTime();
            }
        })
        let query = e
        Http.Post('api/Wx/getPageQr', { query: query, page: `pages/seckill/seckillPage`}).then(res => {
            if (res.code === 1000) {
                this.setData({ url: res.data.all_url })
            } else {
                wx.showToast({
                    title: res.message,
                    icon: 'none'
                })
            }
        })
    },
    getTime() {
        var timestamp = Date.parse(new Date());
        timestamp = timestamp / 1000
        if (this.data.good.sale_start_time < timestamp) {
            if (this.data.good.sale_end_time - timestamp > 0) {
                let time = this.data.good.sale_end_time - timestamp 
                // 获取天、时、分、秒
                let day = parseInt(time / (60 * 60 * 24));
                let hou = parseInt(time % (60 * 60 * 24) / 3600);
                let min = parseInt(time % (60 * 60 * 24) % 3600 / 60);
                let sec = parseInt(time % (60 * 60 * 24) % 3600 % 60);
                var obj = {
                    day: this.timeFormat(day),
                    hou: this.timeFormat(hou),
                    min: this.timeFormat(min),
                    sec: this.timeFormat(sec)
                }
                this.setData({
                    time: obj
                })
                this.setData({ status: 'start' })
            } else {
                this.setData({ status: 'over' })
            }
        } else {
            if (this.data.good.sale_start_time - timestamp > 0) {
                let time = this.data.good.sale_start_time - timestamp
                // 获取天、时、分、秒
                let day = parseInt(time / (60 * 60 * 24));
                let hou = parseInt(time % (60 * 60 * 24) / 3600);
                let min = parseInt(time % (60 * 60 * 24) % 3600 / 60);
                let sec = parseInt(time % (60 * 60 * 24) % 3600 % 60);
                var obj = {
                    day: this.timeFormat(day),
                    hou: this.timeFormat(hou),
                    min: this.timeFormat(min),
                    sec: this.timeFormat(sec)
                }
                this.setData({
                    time: obj
                })
            }
            this.setData({ status: 'overTime' })
        }
        setTimeout(this.getTime, 1000);
    },
    imgLoad (e) {
        this.setData({
            iWidth: e.detail.width,
            iHeight: e.detail.height
        })
    },
    shareStatus () {
        this.shareActive.shareActive()
    }
})