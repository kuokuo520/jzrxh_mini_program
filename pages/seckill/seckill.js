import Http from '../../utils/Api.js'
import Time from '../../utils/time.js'
Page({

    /**
     * 页面的初始数据
     */
    data: {

        secKillIng: true,
        SecKill: false,
        secKills: [],
        voerTime: [],
        startTime: []
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function(options) {
        this.getSecKill()
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function() {},

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function() {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function() {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function() {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function() {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function() {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function() {

    },
    tabLeft: function(e) {
        let active = e.currentTarget.dataset.active
        if (active !== true) {
            this.setData({
                secKillIng: true
            })
            this.setData({
                SecKill: false
            })
        }
        this.getSecKill()
    },
    tabRight: function(e) {
        let active = e.currentTarget.dataset.active
        if (active !== true) {
            this.setData({
                secKillIng: false
            })
            this.setData({
                SecKill: true
            })
        }
        Http.Post('api/Seckill/getGoodsList', {
            status: 1
        }).then(res => {
            if (res.code === -1004) {
                wx.showModal({
                    title: '',
                    content: '需要获取您的用户才能体验所有功能，点击确定跳转登录。',
                    success(res) {
                        if (res.confirm) {
                            wx.switchTab({
                                url: '/pages/my/my',
                            })
                        } else if (res.cancel) {

                        }
                    }
                })
            }
            if (res.code === 1000) {
                this.setData({
                    secKills: res.data
                })
                var countDownArr = [];
                var startTime = []
                for (var i = 0; i < res.data.length; i++) {
                    var actEndTime = res.data[i].sale_start_time
                    var timestamp = Date.parse(new Date());
                    var obj = {}
                    timestamp = timestamp / 1000
                    if (actEndTime - timestamp > 0) {
                        let time = actEndTime - timestamp;
                        // 获取天、时、分、秒
                        let day = parseInt(time / (60 * 60 * 24));
                        let hou = parseInt(time % (60 * 60 * 24) / 3600);
                        let min = parseInt(time % (60 * 60 * 24) % 3600 / 60);
                        let sec = parseInt(time % (60 * 60 * 24) % 3600 % 60);
                        obj.day = this.timeFormat(day)
                        obj.hou = this.timeFormat(hou)
                        obj.min = this.timeFormat(min)
                        obj.sec = this.timeFormat(sec)
                        countDownArr.push(obj)
                    } else { //活动已结束，全部设置为'00'
                        var obj = {
                            day: '00',
                            hou: '00',
                            min: '00',
                            sec: '00'
                        }
                    }
                    startTime.push(obj);
                }
                this.setData({
                    startTime: startTime
                })
            } else {
                this.setData({
                    secKills: ''
                })
            }
        })
    },
    timeFormat(param) { //小于10的格式化函数
        return param < 10 ? '0' + param : param;
    },
    getSecKill() {
        Http.Post('api/Seckill/getGoodsList', {
            status: 2
        }).then(res => {
            if (res.code === -1004) {
                wx.showModal({
                    title: '',
                    content: '需要获取您的用户才能体验所有功能，点击确定跳转登录。',
                    success(res) {
                        if (res.confirm) {
                            wx.switchTab({
                                url: '/pages/my/my',
                            })
                        } else if (res.cancel) {

                        }
                    }
                })
            }
            if (res.code === 1000) {
                this.setData({
                    secKills: res.data
                })
                var countDownArr = [];
                for (let i = 0; i < res.data.length; i++) {
                    var actEndTime = res.data[i].sale_end_time
                    var timestamp = Date.parse(new Date());
                    timestamp = timestamp / 1000
                    var obj = {}
                    if (actEndTime - timestamp > 0) {
                        let time = actEndTime - timestamp;
                        // 获取天、时、分、秒
                        let day = parseInt(time / (60 * 60 * 24));
                        let hou = parseInt(time % (60 * 60 * 24) / 3600);
                        let min = parseInt(time % (60 * 60 * 24) % 3600 / 60);
                        let sec = parseInt(time % (60 * 60 * 24) % 3600 % 60);
                        obj.day = this.timeFormat(day)
                        obj.hou = this.timeFormat(hou)
                        obj.min = this.timeFormat(min)
                        obj.sec = this.timeFormat(sec)
                        countDownArr.push(obj)
                    } else { //活动已结束，全部设置为'00'
                        var obj = {
                            day: '00',
                            hou: '00',
                            min: '00',
                            sec: '00'
                        }
                    }
                }
            } else {
                this.setData({
                    secKills: ''
                })
                wx.showToast({
                    title: res.message,
                    icon: 'none'
                })
            }
            this.setData({
                voerTime: countDownArr
            })
        })
    }
})