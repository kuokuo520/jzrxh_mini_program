import Http from "../../utils/Api.js"
Page({

    /**
     * 页面的初始数据
     */
    data: {
        classId: 0,
        animationData: {},
        type: [{
                id: 0,
                name: '全部'
            },
            {
                id: 1,
                name: '增加积分'
            },
            {
                id: 2,
                name: '减少积分'
            }
        ],
        sort: [{
                id: 1,
                name: '时间倒序'
            },
            {
                id: 2,
                name: '时间升序'
            }
        ],
        size: 1,
        list: []
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function(options) {
        this.getRecord()
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function() {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function() {
        const animation = wx.createAnimation({
            duration: 1000,
            timingFunction: 'ease',
        })
    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function() {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function() {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function() {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function() {
        this.data.size++
            this.getRecord()
    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function() {

    },
    activeMenu(e) {
        let id = e.currentTarget.dataset.id
        if (id === this.data.classId) {
            this.setData({
                classId: 0
            })
        } else {
            this.setData({
                classId: id
            })
        }
    },
    activeType(e) {
        this.setData({
            classId: '',
            typeId: e.currentTarget.dataset.id
        })
        this.getRecord()
    },
    activeSort(e) {
        this.setData({
            classId: '',
            sortId: e.currentTarget.dataset.id
        })
        this.getRecord()
    },
    getRecord() {
        Http.Post('api/Integral/detail', {
            type: this.data.typeId,
            sort: this.data.sortId,
            page: this.data.size
        }).then(res => {
            if (res.code === 1000) {
                console.log(this.data.list.length)
                if (this.data.list.length === 0) {
                    this.setData({
                        list: res.data
                    })
                } else {
                    this.data.list.push(res.data)
                }
            } else {
                wx.showToast({
                    title: res.message,
                })
            }
        })
    }
})