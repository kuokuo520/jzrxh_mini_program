import Http from '../../utils/Api.js'
Page({

    /**
     * 页面的初始数据
     */
    data: {
        item: [{
                title: '好东西',
                credits: 10,
                original_credits: 50,
                thumbnail: 'https://image.weilanwl.com/img/square-3.jpg'
            },
            {
                title: '好东西',
                credits: 10,
                original_credits: 50,
                thumbnail: 'https://image.weilanwl.com/img/square-3.jpg'
            }
        ],
        authStatus: false,
        userInfo: {},
        item: [],
        creditExplain: ''
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function(options) {
        this.getItem()
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function() {
    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function() {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function() {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function() {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function() {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function() {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function() {

    },
    getItem(e) {
        console.log(e)
        Http.Post('api/Goods/getGoodsList').then(res =>{
            if (res.code === 1000) {
                this.setData({item: res.data})
            }
        })
    },
    activeMyItem() {
        wx.navigateTo({
            url: '/pages/my/myItem',
        })
    },
    activeCredit () {
        wx.navigateTo({
            url: '/pages/item/detail',
        })
       
    },
    hideModal(e) {
        this.setData({
            modalName: null
        })
    },
    getUserInfo (e) {
        this.setData({
            userInfo: e.detail
        })
    }
})