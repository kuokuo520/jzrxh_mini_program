import Http from '../../utils/Api.js'
var WxParse = require('../../wxParse/wxParse.js');
Page({

    /**
     * 页面的初始数据
     */
    data: {
        good: []
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function(options) {
        this.getItem(options.id)
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function() {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function() {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function() {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function() {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function() {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function() {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function() {

    },
    getItem(id) {
        Http.Post('api/Goods/getGoodsDetail', {
            goods_id: id
        }).then(res => {
            if (res.code === 1000) {
                this.setData({
                    good: res.data
                })
                WxParse.wxParse('article', 'html', this.data.good.desc, this, 5);
            }
        })
    },
    submit(e) {
        wx.navigateTo({
            url: `/pages/item/itemAddress?id=${this.data.good.id}`,
        })
        // Http.Post('api/Order/exchange', {
        //     goods_id: e.currentTarget.dataset.orderid,
        //     pay_type: 1
        // }).then(res => {
        //     if (res.code === 1000) {
        //         wx.showToast({
        //             title: res.message,
        //             icon: 'none'
        //         })
        //     } else {
        //         wx.showToast({
        //             title: res.message,
        //             icon: 'none'
        //         })
        //     }
        // })
    }
})