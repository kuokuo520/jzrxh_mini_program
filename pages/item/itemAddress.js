import Http from '../../utils/Api.js'
Page({

    /**
     * 页面的初始数据
     */
    data: {
        id: '',
        userName: '',
        userPhone: '',
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        this.setData({id: options.id})
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {
        this.getStorage()
    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    },
    enlist (e) {
        Http.Post('api/Order/exchange', { goods_id: this.data.id, consignee: this.data.userName, mobile: this.data.userPhone}).then(res => {
            if(res.code === 1000) {
                wx.showToast({
                    title: res.message,
                })
                wx.navigateTo({
                    url: '/pages/my/myItem',
                })
                wx.setStorage({
                    key: "userInfo",
                    data: {
                        userName: this.data.userName,
                        userPhone: this.data.userPhone
                    }
                })
            } else {
                wx.showModal({
                    title: '',
                    content: res.message,
                    showCancel: false
                })
            }
            if (res.code === -1004) {
                wx.showModal({
                    title: '',
                    content: '需要获取您的用户才能体验所有功能，点击确定跳转登录。',
                    success(res) {
                        if (res.confirm) {
                            wx.switchTab({
                                url: '/pages/my/my',
                            })
                        } else if (res.cancel) {

                        }
                    }
                })
            }
        })
    },
    putName (e) {
        this.setData({ userName: e.detail.value})
    },
    putPhone (e)  {
        this.setData({ userPhone: e.detail.value })
    },
    getStorage() {
        wx.getStorage({
            key: 'userInfo',
            success: (res) => {
                console.log(res)
                this.setData({
                    userName: res.data.userName,
                    userPhone: res.data.userPhone
                })
            }
        })
    },
    formSubmit(e) {
        let openid = ''
        wx.getStorage({
            key: 'openid',
            success: (res) => {
                openid = res.data
            },
        })
        setTimeout(() => {
            Http.Post('api/Wx/saveFormId', {
                openid: openid,
                formId: e.detail.formId
            }).then(res => {
                console.log(res)
                this.postCoupon()
            })
        }, 500)
    },
    postCoupon() {
        Http.Post('/api/Marketing/pushGoods').then(res => {
            console.log(res)
        })
    }
})