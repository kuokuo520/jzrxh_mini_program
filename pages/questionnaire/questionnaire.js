import Http from "../../utils/Api.js"
const Length = 0
Page({

    /**
     * 页面的初始数据
     */
    options: {
        addGlobalClass: true,
    },
    data: {
        paper: {},
        answer: [],
        title: {}
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function(options) {
        this.getPaper(options.id)
        this.setData({
            id: options.id
        })
        wx.hideShareMenu()
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function() {},

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function() {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function() {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function() {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function() {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function() {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function() {

    },
    getPaper(id) {
        Http.Post('api/Survey/getPaper', {
            paper_id: id
        }).then(res => {
            if (res.code === 1000) {
                var arr = []
                var quest = []
                res.data.questions.forEach((item, index) => {
                    arr.push(item)
                })
                this.setData({
                    paper: arr
                })
                this.setData({
                    title: res.data
                })
                this.data.paper.forEach((item, index) => {
                    quest.push(undefined)
                })
                this.setData({
                    answer: quest
                })
            } else {
                wx.showModal({
                    title: '',
                    content: '问卷已关闭',
                    showCancel: false,
                    success: (res) => {
                        wx.navigateBack({
                            delta: 1
                        })
                    }
                })
            }
        })
    },
    getRadio(e) {
        var data = e.detail.value.split(",")
        let obj = {}
        obj.option_id = []
        obj.question_id = data[1]
        obj.option_id.push(data[0])
        this.data.answer.splice(data[2], 1, obj)
    },
    checkboxChange(e) {
        var data = e.detail.value[0].split(",")
        let index = data[2]
        var obj = {}
        obj.option_id = []
        for (var i in e.detail.value) {
            var data = e.detail.value[i].split(",")
            obj.option_id.push(data[0])
        }
        obj.question_id = data[1]
        obj.option_id.concat(data[0])
        this.data.answer[index] = obj;
    },
    textareaAInput(e) {
        var data = e.currentTarget.dataset.placeholder.split(",")
        let index = data[1]
        var obj = {}
        obj.question_id = data[0]
        obj.option_txt = e.detail.value
        this.data.answer[index] = obj;
    },
    submit(e) {
        if (res.code === -1004) {
            wx.showModal({
                title: '',
                content: '需要获取您的用户才能体验所有功能，点击确定跳转登录。',
                success(res) {
                    if (res.confirm) {
                        wx.switchTab({
                            url: '/pages/my/my',
                        })
                    } else if (res.cancel) {

                    }
                }
            })
        }
        if (this.data.title.questions.length === this.data.answer.length) {
            Http.Post('api/Survey/sub', {
                paper_id: e.currentTarget.dataset.id,
                answer: this.data.answer
            }).then(res => {
                if (res.code === 1000) {
                    if (res.data.type === 1) {
                        wx.showModal({
                            title: res.message,
                            content: `恭喜您获得${res.data.gift_name}`,
                            showCancel: false,
                            success(res) {
                                if (res.confirm) {
                                    wx.navigateTo({
                                        url: '/pages/index/index',
                                    })
                                }
                            }
                        })
                    } else if (res.data.type === 2) {
                        wx.showModal({
                            title: res.message,
                            content: `恭喜您获得${res.data.gift_name}`,
                            showCancel: false,
                            success(res) {
                                if (res.confirm) {
                                    wx.navigateTo({
                                        url: '/pages/my/surveyItem',
                                    })
                                }
                            }
                        })
                    } else if (res.data.type === 3) {
                        wx.showModal({
                            title: res.message,
                            content: `恭喜您获得${res.data.gift_name}`,
                            showCancel: false,
                            success(res) {
                                if (res.confirm) {
                                    wx.navigateTo({
                                        url: '/pages/park/addCoupon',
                                    })
                                }
                            }
                        })
                    } else {
                        wx.showModal({
                            title: res.message,
                            content: `${res.data.gift_name}`,
                            showCancel: false,
                            success(res) {
                                if (res.confirm) {
                                    wx.navigateTo({
                                        url: '/pages/index/index',
                                    })
                                }
                            }
                        })
                    }
                } else {
                    wx.showToast({
                        title: res.message,
                        icon: 'none'
                    })
                    setTimeout(function() {
                        wx.navigateTo({
                            url: '/pages/index/index',
                        })
                    }, 1000);
                }
            })
        } else {
            wx.showModal({
                title: '',
                content: '有题目没有填写，请填写完整后提交',
                showCancel: false
            })
        }
    }
})