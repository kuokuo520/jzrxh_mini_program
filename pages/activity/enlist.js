import Http from '../../utils/Api.js'
Page({

    /**
     * 页面的初始数据
     */
    data: {
        userName: '',
        userPhone: '',
        id: ''
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        this.setData({
            id: options.id
        })
        this.getStorage()
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    },
    enlist() {
        Http.Post('api/Offlineactivity/addOrder', {
            activity_id: this.data.id,
            contacts: this.data.userName,
            contact_tel: this.data.userPhone,
            pay_type: 2
        }).then(res => {
            if (res.code === 1000) {
                if (res.data.money === 0) {
                    wx.showToast({
                        title: '报名成功'
                    });
                    wx.navigateTo({
                        url: '/pages/my/myActivity',
                    })
                } else {
                    wx.requestPayment({
                        timeStamp: res.data.jsapi.timeStamp,
                        nonceStr: res.data.jsapi.nonceStr,
                        package: res.data.jsapi.package,
                        signType: 'MD5',
                        paySign: res.data.jsapi.paySign,
                        success(res) {
                            wx.showToast({
                                title: '支付成功'
                            });
                            wx.navigateTo({
                                url: '/pages/my/myActivity',
                            })
                        },
                        fail(res) {
                            wx.showToast({
                                title: '取消支付',
                            })
                        }
                    })
                }
            } else {
                wx.showModal({
                    title: '报名失败',
                    content: res.message,
                    showCancel: false
                })
            }
        })
        wx.setStorage({
            key: "userInfo",
            data: {
                userName: this.data.userName,
                userPhone: this.data.userPhone
            }
        })
    },
    putName(e) {
        this.setData({
            userName: e.detail.value
        })
    },
    putPhone(e) {
        this.setData({
            userPhone: e.detail.value
        })
    },
    getStorage() {
        wx.getStorage({
            key: 'userInfo',
            success: (res) => {
                console.log(res)
                this.setData({
                    userName: res.data.userName,
                    userPhone: res.data.userPhone
                })
            }
        })
    },
    formSubmit(e) {
        let openid = ''
        wx.getStorage({
            key: 'openid',
            success: (res) => {
                openid = res.data
            },
        })
        setTimeout(() => {
            Http.Post('api/Wx/saveFormId', {
                openid: openid,
                formId: e.detail.formId
            }).then(res => {
                console.log(res)
                this.postCoupon()
            })
        }, 500)
    },
    postCoupon() {
        Http.Post('/api/Marketing/pushGoods').then(res => {
            console.log(res)
        })
    }
})