import Http from '../../utils/Api.js'
var WxParse = require('../../wxParse/wxParse.js');
Page({

    /**
     * 页面的初始数据
     */
    data: {
        activity: {},
        order: ''
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        if (options.id !== undefined) {
            this.getActivity(options.id)
        } else {
            this.getActivity(options.scene)
        }
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {
        this.getOrder()
        this.shareActive = this.selectComponent("#shareActive")
    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    },
    getActivity(id) {
        Http.Post('api/Offlineactivity/getDetail', { activity_id: id }).then(res => {
            if (res.code === 1000) {
                this.setData({ activity: res.data })
                WxParse.wxParse('article', 'html', this.data.activity.desc, this, 5);
                let query = id
                Http.Post('api/Wx/getPageQr', {
                    query: query,
                    page: 'pages/activity/activityPage'
                }).then(res => {
                    if (res.code === 1000) {
                        this.setData({
                            url: res.data.all_url
                        })
                    } else {
                        wx.showToast({
                            title: res.message,
                            icon: 'none'
                        })
                    }
                })
            } else {
                wx.showModal({
                    title: '',
                    content: '需要获取您的用户信息购买物品，点击确定跳转登录。',
                    success(res) {
                        if (res.confirm) {
                            wx.switchTab({
                                url: '/pages/my/my',
                            })
                        } else if (res.cancel) {

                        }
                    }
                })
            }
        });
        this.imgLoad()
    },
    submit() {
        wx.navigateTo({
            url: `/pages/activity/enlist?id=${this.data.activity.id}`,
        })
    },
    getOrder() {
        Http.Post('api/Offlineactivity/getOrderList', {
        }).then(res => {
            if (res.code === 1000) {
                this.setData({
                    order: res.data.length
                })
            } else {
                this.setData({
                    order: ''
                })
            }
        })
    },
    shareStatus() {
        this.shareActive.shareActive()
    },
    imgLoad(e) {
        this.setData({
            iWidth: e.detail.width,
            iHeight: e.detail.height
        })
    }
})