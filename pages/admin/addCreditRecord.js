import Http from '../../utils/Api.js'
Page({

    /**
     * 页面的初始数据
     */
    data: {
        times: []
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        this.getRecord()
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    },
    getRecord () {
        Http.Post('api/Integral/history').then(res => {
            if (res.code === 1000) {
                this.setData({list: res.data})
                var times = []
                for (var i = 0; i < res.data.length; i++) {
                    let time = {}
                    time.date = res.data[i].create_time.substring(0, 10)
                    time.time = res.data[i].create_time.substring(10, 20)
                    times.push(time)
                }
                this.setData({times: times})
            } else {
                wx.showToast({
                    title: res.message,
                    icon: 'none'
                })
                this.setData({
                    list: ''
                })
            }
        })
    }
})