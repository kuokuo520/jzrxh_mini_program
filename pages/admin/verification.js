// pages/shop/shop.js
Page({

    /**
     * 页面的初始数据
     */
    data: {
        classId: 0,
        animationData: {},
        record: []
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        this.getRecord()
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {
        const animation = wx.createAnimation({
            duration: 1000,
            timingFunction: 'ease',
        })
    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    },
    activeMenu(e) {
        let id = e.currentTarget.dataset.id
        if (id === this.data.classId) {
            this.setData({
                classId: 0
            })
        } else {
            this.setData({
                classId: id
            })
        }
    },
    getRecord () {
        Http.Post('/api/WriteOff/sendRecord', {}).then(res => {
            this.setData({
                record: res.data.data
            })
        })
    }
})