import Http from '../../utils/Api.js'
Page({

    /**
     * 页面的初始数据
     */
    data: {

    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function(options) {

    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function() {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function() {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function() {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function() {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function() {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function() {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function() {

    },
    getPhone(e) {
        let data = e.detail.value
        this.setData({
            userPhone: data
        })
    },
    textareaBInput(e) {
        let data = e.detail.value
        this.setData({
            userB: data
        })
    },
    getCredit(e) {
        let data = e.detail.value
        this.setData({
            userCredit: data
        })
    },
    submit() {
        Http.Post('api/Integral/add', {
            integral: this.data.userCredit,
            telphone: this.data.userPhone,
            remarks: this.data.userB,
            type: 1
        }).then(res => {
            if (res.code === 1000) {
                wx.showToast({
                    title: res.message,
                })
                this.setData({
                    integral: '',
                    telphone: '',
                    remarks: ''})
            }
        })
    },
    scop () {
        wx.scanCode({
            success(res) {
                console.log(res)
            }
        }) 
    }
})