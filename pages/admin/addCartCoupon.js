import Http from '../../utils/Api.js'
Page({

    /**
     * 页面的初始数据
     */
    data: {
        userPhone: '',
        remarks: '',
        coupon: [],
        couponIndex: -1
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function(options) {
        this.getCoupon()
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function() {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function() {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function() {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function() {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function() {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function() {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function() {

    },
    getPhone(e) {
        let data = e.detail.value
        this.setData({
            userPhone: data
        })
    },
    textareaBInput (e) {
        let data = e.detail.value
        this.setData({
            remarks: data
        })
    },
    submit() {
        Http.Post('/api/WriteOff/send', {
            coupon_id: this.data.coupon[this.data.couponIndex].id,
            account: this.data.userPhone,
            remarks: this.data.remarks
        }).then(res => {
            wx.showModal({
                title: '',
                content: res.message,
                showCancel: false
            })
            this.setData({
                userPhone: '',
                remarks: '',
                couponIndex: -1
            })
        })
    },
    getCoupon () {
        Http.Post('/api/Coupon/getAllCouponList').then(res => {
            this.setData({
                coupon: res.data
            })
        })
    },
    scop() {
        wx.scanCode({
            success: (res) => {
                this.setData({
                    userPhone: res.result
                })
            }
        })
    },
    PickerChange (e) {
        console.log(e)
        this.setData({
            couponIndex: e.detail.value
        })
    }
})