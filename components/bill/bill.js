Component({
    /**
     * 组件的属性列表
     */
    options: {
        addGlobalClass: true,
    },
    canvasIdErrorCallback: function(e) {
        console.error(e.detail.errMsg)
    },
    properties: {
        good: {
            type: Object
        },
        iWidth: {
            type: String
        },
        iHeight: {
            type: String
        },
        url: {
            type: String
        }
    },

    /**
     * 组件的初始数据
     */
    data: {
        modalName: '',
        windowH: '',
        tempCanvas: '',
        canvasStatus: true
    },
    /**
     * 组件的方法列表
     */
    pageLifetimes: {
        show: function() {
			wx.getSystemInfo({
				success: (res) => {
					console.log(res)
					var windowWidth = res.windowWidth;
					var windowHeight = res.windowHeight;
					this.setData({
						windowW: windowWidth,
						windowH: windowHeight
					})
				}
			})
        },
        hide: function() {
            // 页面被隐藏
        },
        resize: function(size) {
            // 页面尺寸变化
        }
    },
    methods: {
        drawForecastView(e) {
            var data = e.currentTarget.dataset.obj
            var imageSize = {};
            var bannerSrc = ''
            var originalWidth = e.currentTarget.dataset.width; //图片原始宽  
            var originalHeight = e.currentTarget.dataset.height; //图片原始高  
            var originalScale = originalHeight / originalWidth;
            //获取屏幕宽高  
            wx.getSystemInfo({
                success: (res) => {
                    console.log(res)
                    var windowWidth = res.windowWidth;
                    var windowHeight = res.windowHeight;
                    this.setData({
                        windowW: windowWidth,
                        windowH: windowHeight
                    })
                    var windowscale = windowHeight / windowWidth;
                    if (originalScale < windowscale) { //图片高宽比小于屏幕高宽比  
                        //图片缩放后的宽为屏幕宽  
                        imageSize.imageWidth = windowWidth;
                        imageSize.imageHeight = (windowWidth * originalHeight) / originalWidth;
                    } else { //图片高宽比大于屏幕高宽比  
                        //图片缩放后的高为屏幕高  
                        imageSize.imageHeight = windowHeight;
                        imageSize.imageWidth = (windowHeight * originalWidth) / originalHeight;
                    }
                    console.log(imageSize)
                }
            })
            if (e.currentTarget.dataset.obj.goods_name === undefined) {
                data.goods_name = e.currentTarget.dataset.obj.name
            }
            if (e.currentTarget.dataset.obj.goods_money === undefined) {
                data.goods_money = e.currentTarget.dataset.obj.sale_money
            }
            var context = wx.createCanvasContext('firstCanvas', this);
            context.setFillStyle('white')
            context.fillRect(0, 0, this.data.windowW, this.data.windowH - 300)
            context.drawImage(this.data.bannerImageSrc, 0, 0, imageSize.imageWidth, imageSize.imageHeight)
            context.setFontSize(24) //设置字体大小，默认10
            context.setFillStyle('#000')
            context.fillText(data.goods_name, 10, imageSize.imageHeight + 50) //绘制文本
            context.setFillStyle('#ff0000')
            context.fillText('￥', 10, imageSize.imageHeight + 90) //绘制文本
            context.fillText(data.goods_money, 30, imageSize.imageHeight + 90) //绘制文本
            context.rect(10, imageSize.imageHeight + 120, imageSize.imageWidth - 20, 1)
            context.setFillStyle('#e3e3e3')
            context.fill()
            context.setFontSize(20)
            context.setFillStyle('#666')
            context.fillText('长按识别小程序码访问', 10, imageSize.imageHeight + 170) //绘制文本
            context.setFontSize(16)
            context.setFillStyle('#999')
            context.fillText('荆州人信汇小程序', 10, imageSize.imageHeight + 230) //绘制文本
            context.drawImage(this.data.qrCodeImg, imageSize.imageWidth - 120, imageSize.imageHeight + 140, 110, 110)
            context.draw()
            setTimeout(() => {
                wx.canvasToTempFilePath({
                    x: 0,
                    y: 0,
                    canvasId: 'firstCanvas',
                    fileType: 'jpg',
                    quality: 1,
                    success: (res) => {
                        this.setData({
                            tempCanvas: res.tempFilePath
                        })
                        maskHidden: false
                        this.setData({
                            canvasStatus: false
                        })
                    }
                }, this)
            }, 2000);
            this.setData({
                modalName: 'DialogModal1',
            })
        },
        saveQrCode() {
            wx.saveImageToPhotosAlbum({
                filePath: this.data.tempCanvas,
                success: (res) => {
                    wx.showToast({
                        title: '保存成功'
                    })
                    this.setData({
                        modalName: ''
                    })
                }
            })
        },
        hideModal() {
            this.setData({
                modalName: ''
            })
        },
        shareFired() {
            wx.updateShareMenu({
                withShareTicket: true,
                success() {}
            })
            console.log('233')
        },
        shareActive() {
            this.setData({
                modalName: 'bottomModal'
            })
        },
        saveImage(e) {
            wx.showToast({
                title: '分享图片生成中...',
                icon: 'loading',
                duration: 3000
            });
            if (e.currentTarget.dataset.obj.goods_img === undefined) {
                e.currentTarget.dataset.obj.goods_img = e.currentTarget.dataset.obj.main_pic
            }
            //下载海报图
            wx.downloadFile({
                url: e.currentTarget.dataset.obj.goods_img,
                success: (res) => {
                    this.setData({
                        bannerImageSrc: res.tempFilePath
                    })
                },
                fail: function() {
                    console.log('fail')
                }
            })
            //下载二维码
            wx.downloadFile({
                url: e.currentTarget.dataset.url,
                success: (res) => {
                    this.setData({
                        qrCodeImg: res.tempFilePath
                    })
                },
                fail: function() {
                    console.log('fail')
                }
            })
            setTimeout(() => {
                this.drawForecastView(e)
            }, 1000);
        }
    }
})