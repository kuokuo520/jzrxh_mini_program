// components/shop/shopList.js
Component({
    /**
     * 组件的属性列表
     */
    options: {
        addGlobalClass: true,
    },
    properties: {
        shopList: {
            type: Object
        },
        isLoad: {
            type: String
        },
        line: {
            type: Boolean
        }
    },

    /**
     * 组件的初始数据
     */
    data: {
       
    },
    pageLifetimes: {
        show: function () {
            this.activeLoad()
        },
        hide: function () {
            // 页面被隐藏
        },
        resize: function (size) {
            // 页面尺寸变化
        }
    },
    /**
     * 组件的方法列表
     */
    methods: {
        activePhone(e) {
            wx.makePhoneCall({
                phoneNumber: e.currentTarget.dataset.phone //仅为示例，并非真实的电话号码
            })
        },
        activeLoad (e) {
            if (e === undefined) {
                this.setData({
                    isLoad: 'over'
                })
                console.log(this.properties.isLoad)
            }
        }
    }
})