import Http from '../../utils/Api.js'
var WxParse = require('../../wxParse/wxParse.js');
Component({
    /**
     * 组件的属性列表
     */
    options: {
        addGlobalClass: true,
    },
    properties: {
        status: {
            type:String
        },
        startLottery: {

            type: Array
        },
        myLottery: {
            type: Array
        }
    },

    /**
     * 组件的初始数据
     */
    data: {
        status: '',
        myItem: {}
    },
    created: function () {
        this.getZhuanPan()
    },
    /**
     * 组件的方法列表
     */
    methods: {
        close() {
            this.setData({
                status: ''
            })
        },
        exchange (id) {
            Http.Post('api/Lottery/getLotteryCode', { lottery_record_id: id.currentTarget.dataset.id}).then(res => {
                if (res.code === 1000) {
                    this.setData({ myItem: res.data })
                    this.setData({ modalName: 'Modal'})
                }
            })
        },
        hideModal () {
            this.setData({ modalName: '' })
        },
        getZhuanPan() {
            Http.Post('api/Lottery/get_lottery_activity').then(res => {
                if (res.code === -1000) {
                    wx.showModal({
                        title: '转盘抽奖',
                        content: '敬请期待',
                        showCancel: false,
                        success: res => {
                            if (res.confirm) {
                                wx.switchTab({
                                    url: '/pages/index/index',
                                })
                            }
                        }
                    })
                } else {
                    WxParse.wxParse('article', 'html', res.data.desc, this, 5);
                }
            })
        }
    }
})