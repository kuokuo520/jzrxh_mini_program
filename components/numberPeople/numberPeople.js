import Http from '../../utils/Api.js'
Component({
    options: {
        addGlobalClass: true,
    },
    /**
     * 组件的属性列表
     */
    properties: {
        numberModel: {
            type: String
        },
        animated: {
            type: String
        },
        pageId: {
            type: String
        }
    },

    /**
     * 组件的初始数据
     */
    data: {
        numberModel: false,
        numberValue: '',
        animated: '',
        id: ''
    },

    /**
     * 组件的方法列表
     */
    methods: {
        exit() {
            this.setData({
                animated: 'animated fadeIn',
                numberModel: false,
                numberValue: '',
            })
        },
        submit() {
            var id = this.data.pageId
            Http.Post('api/Quenue/takeNum', {
                person_num: this.data.numberValue,
                shop_id: this.data.pageId
            }).then(res => {
                if (res.code === 1000) {
                    wx.showToast({
                        title: res.message
                    })
                    setTimeout(function() {
                        wx.navigateTo({
                            url: `/pages/line/linePage?id=${id}`,
                        })
                    }, 1000)
                } else if (res.code === -1101) {
                    wx.showToast({
                        title: res.message,
                        icon: 'none'
                    })
					Http.Post('api/Quenue/getMyQuenue').then(res => {
						let index = res.data.length
						index--
						setTimeout(function () {
							wx.navigateTo({
								url: `/pages/line/linePage?id=${res.data[index].shop_id}`,
							})
						}, 1000)
					})
                }
            })
        },
        getNumber(e) {
            let value = e.detail.value
            console.log(e)
            this.setData({
                numberValue: value
            })
        },
        submitInfo (e) {
            wx.getStorage({
                key: 'openid',
                success:(res) => {
                    this.setData({
                        openid: res.data
                    })
                }
            })
            Http.Post('api/Wx/saveFormId', { openid: this.data.openid, formId: e.detail.formId}).then(res => {
                if (res.code === 1000) {
                    this.submit()
                } else {
                    wx.showToast({
                        title: res.message,
                    })
                }
            })
        },
    }
})