import Http from '../../utils/Api.js'
Component({
    /**
     * 组件的属性列表
     */
    properties: {

    },

    /**
     * 组件的初始数据
     */
    data: {
        lineStatus: false,
        lineInfo: {}
    },

    /**
     * 组件的方法列表
     */
    lifetimes: {
        attached: function() {
            this.getLine()
        },
        detached: function() {
            // 在组件实例被从页面节点树移除时执行
        },
    },
    methods: {
        getLine() {
            Http.Post('api/Quenue/getMyQuenue').then(res => {
				let arr = res.data.length - 1 
				if (res.data[arr].status === 1 || res.data[arr].status === 2) {
                    this.setData({
                        lineStatus: true,
                        lineInfo: res.data
                    })
                } else {
                    this.setData({
                        lineStatus: false
                    })
                }
            })
        }
    }
})