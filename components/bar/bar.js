var app = getApp();
Component({
    options: {
        addGlobalClass: true,
    },
    /**
     * 组件的属性列表
     */
    properties: {

    },

    /**
     * 组件的初始数据
     */
    data: {

    },
    /**
     * 组件的方法列表
     */
    methods: {
        activation(e) {
            var url = e.currentTarget.dataset.name
            console.log(url)
            switch (url) {
                case 'index':
                    wx.setStorageSync('barName', 'index')
                    break;
                case 'story':
                    wx.setStorageSync('barName', 'story')
                    break;
                case 'activity':
                    wx.setStorageSync('barName', 'activity')
                    break;
                case 'my':
                    wx.setStorageSync('barName', 'my')
                    break;
            }

        },
    },
    lifetimes: {
        attached() {
            try {
                const value = wx.getStorageSync('barName')
                if (value) {
                    this.setData({
                        barName: value
                    })
                }
            } catch (e) {

            }
        },
        detached() {
            try {

            } catch (e) {
                console.log(e)
            }
        },
    },
})