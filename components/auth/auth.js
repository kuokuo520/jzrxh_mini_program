import Http from '../../utils/Api.js'
Component({
    options: {
        addGlobalClass: true,
    },
    /**
     * 组件的属性列表
     */
    properties: {

    },

    /**
     * 组件的初始数据
     */
    data: {
        modalName: '',
        userInfo: {},
        userHeaderImg: '',
        userName: ''
    },

    /**
     * 组件的方法列表
     */
    pageLifetimes: {
        show() {
           
        }
    },
    methods: {
        getUserInfo() {
            Http.Post('api/user/userinfo').then(res => {
                if (res.code === -1004) {
                    this.setData({
                        modalName: 'DialogModal1',
                    })
                } else {
                    this.setData({
                        userInfo: res.data
                    })
                    this.triggerEvent('getUserInfo', this.data.userInfo) //myevent自定义名称事件，父组件中使用
                }
            })
        },
        onGotUserInfo(e) {
            let info = JSON.parse(e.detail.rawData)
            this.setData({
                avatarUrl: info.avatarUrl,
                userName: info.nickName
            })
            wx.login({
                success: (res) => {
                    if (res.code) {
                        Http.Post('api/Wx/getUserOpenid', {
                            code: res.code
                        }).then(res => {    
                            if (res.code === 1000) {
                                this.setData({
                                    openid: res.data.openid,
                                    session_key: res.data.session_key,
                                    modalName: 'DialogModal11'
                                })
                                wx.setStorage({
                                    key: "openid",
                                    data: res.data.openid
                                })
                            }
                        })
                    } else {
                        console.log('登录失败！' + res.errMsg)
                    }
                }
            })
        },
        getUserPhone(e) {
            Http.Post('api/Wx/aesPhoneDecrypt', {
                session_key: this.data.session_key,
                data: e.detail.encryptedData,
                iv: e.detail.iv
            }).then(res => {
                Http.Post('/api/Login/login', {
                    phone: res.data.phoneNumber,
                    openid: this.data.openid,
                    login_type: 1
                }).then(res => {
                    if (res.code === 1000) {
                        wx.showToast({
                            title: res.message,
                        })
                        this.setData({
                            modalName: ''
                        })
                        wx.setStorage({
                            key: 'Token',
                            data: res.data.token
                        })
                        setTimeout(()=> {
                            this.getUserInfo()
                            Http.Post('api/User/editUserInfo', {
                                headimg: this.data.avatarUrl,
                                nickname: this.data.userName
                            })
                            wx.switchTab({
								url: '/pages/index/index',
							})
                         }, 2000);
                    }
                })
            })
        },
        hideModal() {
            this.setData({
                modalName: ''
            })
            wx.showToast({
                title: '接受授权体验功能'
            })
            wx.switchTab({
                url: '/pages/index/index',
            })
        }
    },
})