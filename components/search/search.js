import Http from '../../utils/Api.js'
Component({
    /**
     * 组件的属性列表
     */
    options: {
        addGlobalClass: true,
    },
    properties: {

    },

    /**
     * 组件的初始数据
     */
    data: {
        searchBg: false
    },

    /**
     * 组件的方法列表
     */
    methods: {
        search(e) {
            let searchText = e.detail.value
            Http.Post('api/Shop/shopList', {
                shop_name: searchText
            }).then(res => {
                if (res.code === 1000) {
                    this.setData({
                        shopList: res.data
                    })
                    console.log(this.data.shopList)
                } else if (res.code === -1000) {
                    wx.showToast({
                        title: res.message,
                        icon: 'none'
                    })
                }
                this.triggerEvent('getShopList', res.data) //myevent自定义名称事件，父组件中使用
            })
        },
        focus() {
            this.setData({
                searchBg: true
            })
        },
        close() {
            this.setData({
                searchBg: false
            })
        },
        activeSearch() {
            wx.navigateTo({
                url: '/pages/search/search',
            })
        }
    }
})