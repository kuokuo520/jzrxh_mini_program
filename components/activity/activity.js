// pages/component/activity/activity.js

Component({
    options: {
        addGlobalClass: true,
    },
    /**
     * 组件的属性列表
     */
    properties: {
        activitys: {
            type: Array
        },
    },

    /**
     * 组件的初始数据
     */
    data: {
        overTime: ''
    },

    /**
     * 组件的方法列表
     */
    methods: {

    },
})