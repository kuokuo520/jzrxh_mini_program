import Http from '../../utils/Api.js'
const arr = []
Component({
    options: {
        addGlobalClass: true,
    },
    /**
     * 组件的属性列表
     */
    properties: {

    },

    /**
     * 组件的初始数据
     */
    data: {
        classify: [],
        TabId: '0',
        good: [],
		size: 1
    },

    /**
     * 组件的方法列表
     */
    methods: {
        activeTab(e) {
            if (e.currentTarget.dataset.id !== 0) {
                this.setData({
                    TabId: e.currentTarget.dataset.id
                })
            } else {
                this.setData({
                    TabId: 0
                })
            }
            this.getCoupon(this.data.TabId)
        },
        getCouponClassify() {
            Http.Post('api/Goods/getGoodsType', {
                level: 2
            }).then(res => {
                if (res.code === 1000) {
                    this.setData({
                        classify: res.data
                    })
                }
            })
        },
        getCoupon(id) {
            if (this.data.size === 1) {
				Http.Post('api/Goods/getGoodsList', {
					type: id,
					goods_type: 3,
				}).then(res => {
					this.setData({
						good: res.data
					})
					res.data.forEach((item => {
						arr.push(item)
					}))
				})
				this.data.size++
			} else {
				Http.Post('api/Goods/getGoodsList', {
					type: id,
					goods_type: 3,
					page: this.data.size
				}).then(res => {
					res.data.forEach((item => {
						arr.push(item)
					}))
					this.setData({
						good: arr
					})
				})
				this.data.size++
			}
        }
    },
    pageLifetimes: {
        show: function() {
            this.getCouponClassify()
            this.getCoupon()
        },
    },
})