const API_URL = "https://jzrxh.xiaolu-media.com/";


function Get(url, params) {

    let promise = new Promise(function(resolve, reject) {

        wx.request({

            url: API_URL + url,

            data: params,

            method: 'GET',

            header: {
                'Content-Type': 'application/json'
            },

            success: res => {

                resolve(res.data);

            },

            fail: res => {

                reject(res.data)

            }

        })

    });

    return promise

}

function Post(url, params, token) {

    let promise = new Promise(function(resolve, reject) {
        wx.request({

            url: API_URL + url,

            data: params,

            method: 'POST',

            header: {
                'content-Type': 'application/json',
                'token': wx.getStorageSync('Token')
            },

            success: res => {
                resolve(res.data);
            },

            fail: res => {
                reject(res.data)
            }

        })

    });

    return promise

}

function JsonPost(url, params) {

    let promise = new Promise(function(resolve, reject) {

        wx.request({

            url: API_URL + url,

            data: JSON.stringify(params),

            method: 'POST',

            header: {
                'Content-Type': 'application/json'
            },

            success: res => {

                resolve(res.data);

            },

            fail: res => {

                reject(res.data);

            }

        })

    });

    return promise

}

function WxLogin(openid, avatarUrl, gender, nickName) {
    Post('api/Login/login', {
        openid,
        avatarUrl,
        gender,
        nickName
    }).then(res => {
        if (res.code === 1000) {
            wx.showToast({
                title: '登陆成功',
            })
            wx.setStorage({
                key: 'Token',
                data: res.data.token
            })
        }
    })

}

module.exports = {

    Get,

    Post,

    JsonPost,

    WxLogin,
    API_URL
}